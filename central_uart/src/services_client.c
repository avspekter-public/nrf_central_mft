/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>

#include "services.h"
#include "services_client.h"

// #include <bluetooth/services/nus.h>
// #include <bluetooth/services/nus_client.h>


#include <zephyr/logging/log.h>

#define LOG_MODULE_NAME services_client
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

enum {
	NUS_C_INITIALIZED,
	NUS_C_TEST_CONF_NOTIF_ENABLED,
	NUS_C_READINGS_NOTIF_ENABLED,
	NUS_C_STATUS_NOTIF_ENABLED,
	NUS_C_RESULT_AVAILABLE_NOTIF_ENABLED,
	//NUS_C_RX_WRITE_PENDING
};

static uint8_t on_test_conf_received(struct bt_conn *conn, struct bt_gatt_subscribe_params *params,
				     const void *data, uint16_t length)
{
	struct bt_nus_client *nus;

	/* Retrieve NUS Client module context. */
	nus = CONTAINER_OF(params, struct bt_nus_client, test_conf_notif_params);

	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0;
		atomic_clear_bit(&nus->test_conf_state, NUS_C_TEST_CONF_NOTIF_ENABLED);
		if (nus->cb.test_conf_unsubscribed) {
			nus->cb.test_conf_unsubscribed(nus);
		}
		return BT_GATT_ITER_STOP;
	}

	LOG_DBG("[NOTIFICATION] data %p length %u", data, length);
	if (nus->cb.test_conf_received) {
		return nus->cb.test_conf_received(nus, data, length);
	}

	return BT_GATT_ITER_CONTINUE;
}

static uint8_t on_readings_received(struct bt_conn *conn, struct bt_gatt_subscribe_params *params,
				    const void *data, uint16_t length)
{
	struct bt_nus_client *nus;

	/* Retrieve NUS Client module context. */
	nus = CONTAINER_OF(params, struct bt_nus_client, readings_notif_params);

	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0;
		atomic_clear_bit(&nus->readings_state, NUS_C_READINGS_NOTIF_ENABLED);
		if (nus->cb.readings_unsubscribed) {
			nus->cb.readings_unsubscribed(nus);
		}
		return BT_GATT_ITER_STOP;
	}

	LOG_DBG("[NOTIFICATION] data %p length %u", data, length);
	if (nus->cb.readings_received) {
		return nus->cb.readings_received(nus, data, length);
	}

	return BT_GATT_ITER_CONTINUE;
}

static uint8_t on_status_received(struct bt_conn *conn, struct bt_gatt_subscribe_params *params,
				  const void *data, uint16_t length)
{
	struct bt_nus_client *nus;

	/* Retrieve NUS Client module context. */
	nus = CONTAINER_OF(params, struct bt_nus_client, status_notif_params);

	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0;
		atomic_clear_bit(&nus->status_state, NUS_C_STATUS_NOTIF_ENABLED);
		if (nus->cb.status_unsubscribed) {
			nus->cb.status_unsubscribed(nus);
		}
		return BT_GATT_ITER_STOP;
	}

	LOG_DBG("[NOTIFICATION] data %p length %u", data, length);
	if (nus->cb.status_received) {
		return nus->cb.status_received(nus, data, length);
	}

	return BT_GATT_ITER_CONTINUE;
}

static uint8_t on_result_available_received(struct bt_conn *conn,
					    struct bt_gatt_subscribe_params *params,
					    const void *data, uint16_t length)
{
	struct bt_nus_client *nus;

	/* Retrieve NUS Client module context. */
	nus = CONTAINER_OF(params, struct bt_nus_client, result_available_notif_params);

	if (!data) {
		LOG_DBG("[UNSUBSCRIBED]");
		params->value_handle = 0;
		atomic_clear_bit(&nus->result_available_state, NUS_C_RESULT_AVAILABLE_NOTIF_ENABLED);
		if (nus->cb.result_available_unsubscribed) {
			nus->cb.result_available_unsubscribed(nus);
		}
		return BT_GATT_ITER_STOP;
	}

	LOG_DBG("[NOTIFICATION] data %p length %u", data, length);
	if (nus->cb.result_available_received) {
		return nus->cb.result_available_received(nus, data, length);
	}

	return BT_GATT_ITER_CONTINUE;
}

// static void on_sent(struct bt_conn *conn, uint8_t err,
// 		    struct bt_gatt_write_params *params)
// {
// 	struct bt_nus_client *nus_c;
// 	const void *data;
// 	uint16_t length;

// 	/* Retrieve NUS Client module context. */
// 	nus_c = CONTAINER_OF(params, struct bt_nus_client, rx_write_params);

// 	/* Make a copy of volatile data that is required by the callback. */
// 	data = params->data;
// 	length = params->length;

// 	atomic_clear_bit(&nus_c->state, NUS_C_RX_WRITE_PENDING);
// 	if (nus_c->cb.sent) {
// 		nus_c->cb.sent(nus_c, err, data, length);
// 	}
// }

int bt_nus_client_init(struct bt_nus_client *nus_c,
		       const struct bt_nus_client_init_param *nus_c_init)
{
	if (!nus_c || !nus_c_init) {
		return -EINVAL;
	}

	if (atomic_test_and_set_bit(&nus_c->test_conf_state, NUS_C_INITIALIZED)) {
		return -EALREADY;
	}

		if (atomic_test_and_set_bit(&nus_c->readings_state, NUS_C_INITIALIZED)) {
		return -EALREADY;
	}

		if (atomic_test_and_set_bit(&nus_c->status_state, NUS_C_INITIALIZED)) {
		return -EALREADY;
	}

		if (atomic_test_and_set_bit(&nus_c->result_available_state, NUS_C_INITIALIZED)) {
		return -EALREADY;
	}

	memcpy(&nus_c->cb, &nus_c_init->cb, sizeof(nus_c->cb));
	

	return 0;
}

// int bt_nus_client_send(struct bt_nus_client *nus_c, const uint8_t *data,
// 		       uint16_t len)
// {
// 	int err;

// 	if (!nus_c->conn) {
// 		return -ENOTCONN;
// 	}

// 	if (atomic_test_and_set_bit(&nus_c->state, NUS_C_RX_WRITE_PENDING)) {
// 		return -EALREADY;
// 	}

// 	nus_c->rx_write_params.func = on_sent;
// 	nus_c->rx_write_params.handle = nus_c->handles.rx;
// 	nus_c->rx_write_params.offset = 0;
// 	nus_c->rx_write_params.data = data;
// 	nus_c->rx_write_params.length = len;

// 	err = bt_gatt_write(nus_c->conn, &nus_c->rx_write_params);
// 	if (err) {
// 		atomic_clear_bit(&nus_c->state, NUS_C_RX_WRITE_PENDING);
// 	}

// 	return err;
// }

int bt_nus_handles_assign(struct bt_gatt_dm *dm, struct bt_nus_client *nus_c)
{
	const struct bt_gatt_dm_attr *gatt_service_attr = bt_gatt_dm_service_get(dm);
	const struct bt_gatt_service_val *gatt_service =
		bt_gatt_dm_attr_service_val(gatt_service_attr);
	const struct bt_gatt_dm_attr *gatt_chrc;
	const struct bt_gatt_dm_attr *gatt_desc;

	if (bt_uuid_cmp(gatt_service->uuid, BT_UUID_LIVE_UPDATE_SERVICE)) {
		return -ENOTSUP;
	}
	LOG_DBG("Getting handles from NUS service.");
	memset(&nus_c->handles, 0xFF, sizeof(nus_c->handles));

	/* test conf Characteristic */
	gatt_chrc = bt_gatt_dm_char_by_uuid(dm, BT_UUID_TEST_CONF);
	if (!gatt_chrc) {
		LOG_ERR("Missing test conf characteristic.");
		return -EINVAL;
	}

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_TEST_CONF);
	if (!gatt_desc) {
		LOG_ERR("Missing test conf value descriptor in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for test conf characteristic.");
	nus_c->handles.test_conf = gatt_desc->handle;

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_GATT_CCC);
	if (!gatt_desc) {
		LOG_ERR("Missing test conf CCC in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for CCC of test conf characteristic.");
	nus_c->handles.test_conf_ccc = gatt_desc->handle;

	/* readings Characteristic */
	gatt_chrc = bt_gatt_dm_char_by_uuid(dm, BT_UUID_READINGS);
	if (!gatt_chrc) {
		LOG_ERR("Missing readings characteristic.");
		return -EINVAL;
	}

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_READINGS);
	if (!gatt_desc) {
		LOG_ERR("Missing readings value descriptor in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for readings characteristic.");
	nus_c->handles.readings = gatt_desc->handle;

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_GATT_CCC);
	if (!gatt_desc) {
		LOG_ERR("Missing readings CCC in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for CCC of readings characteristic.");
	nus_c->handles.readings_ccc = gatt_desc->handle;

	/* status Characteristic */
	gatt_chrc = bt_gatt_dm_char_by_uuid(dm, BT_UUID_STATUS);
	if (!gatt_chrc) {
		LOG_ERR("Missing status characteristic.");
		return -EINVAL;
	}

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_STATUS);
	if (!gatt_desc) {
		LOG_ERR("Missing status value descriptor in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for status characteristic.");
	nus_c->handles.status = gatt_desc->handle;

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_GATT_CCC);
	if (!gatt_desc) {
		LOG_ERR("Missing status CCC in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for CCC of status characteristic.");
	nus_c->handles.status_ccc = gatt_desc->handle;

	/* result available Characteristic */
	gatt_chrc = bt_gatt_dm_char_by_uuid(dm, BT_UUID_RESULT_AVAILABLE);
	if (!gatt_chrc) {
		LOG_ERR("Missing result available characteristic.");
		return -EINVAL;
	}

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_RESULT_AVAILABLE);
	if (!gatt_desc) {
		LOG_ERR("Missing result available value descriptor in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for result available characteristic.");
	nus_c->handles.result_available = gatt_desc->handle;

	gatt_desc = bt_gatt_dm_desc_by_uuid(dm, gatt_chrc, BT_UUID_GATT_CCC);
	if (!gatt_desc) {
		LOG_ERR("Missing result available CCC in characteristic.");
		return -EINVAL;
	}
	LOG_DBG("Found handle for CCC of result available characteristic.");
	nus_c->handles.result_available_ccc = gatt_desc->handle;

	/* Assign connection instance. */
	nus_c->conn = bt_gatt_dm_conn_get(dm);
	return 0;
}

int bt_nus_subscribe_receive(struct bt_nus_client *nus_c)
{
	int err;

	/* test conf */
	if (atomic_test_and_set_bit(&nus_c->test_conf_state, NUS_C_TEST_CONF_NOTIF_ENABLED)) {
		return -EALREADY;
	}

	nus_c->test_conf_notif_params.notify = on_test_conf_received;
	nus_c->test_conf_notif_params.value = BT_GATT_CCC_NOTIFY;
	nus_c->test_conf_notif_params.value_handle = nus_c->handles.test_conf;
	nus_c->test_conf_notif_params.ccc_handle = nus_c->handles.test_conf_ccc;
	atomic_set_bit(nus_c->test_conf_notif_params.flags, BT_GATT_SUBSCRIBE_FLAG_VOLATILE);

	err = bt_gatt_subscribe(nus_c->conn, &nus_c->test_conf_notif_params);
	if (err) {
		LOG_ERR("Subscribe test conf failed (err %d)", err);
		atomic_clear_bit(&nus_c->test_conf_state, NUS_C_TEST_CONF_NOTIF_ENABLED);
		return err;
	} else {
		LOG_DBG("[TEST CONF SUBSCRIBED]");
	}

	/* readings */
	if (atomic_test_and_set_bit(&nus_c->readings_state, NUS_C_READINGS_NOTIF_ENABLED)) {
		return -EALREADY;
	}

	nus_c->readings_notif_params.notify = on_readings_received;
	nus_c->readings_notif_params.value = BT_GATT_CCC_NOTIFY;
	nus_c->readings_notif_params.value_handle = nus_c->handles.readings;
	nus_c->readings_notif_params.ccc_handle = nus_c->handles.readings_ccc;
	atomic_set_bit(nus_c->readings_notif_params.flags, BT_GATT_SUBSCRIBE_FLAG_VOLATILE);

	err = bt_gatt_subscribe(nus_c->conn, &nus_c->readings_notif_params);
	if (err) {
		LOG_ERR("Subscribe readings failed (err %d)", err);
		atomic_clear_bit(&nus_c->readings_state, NUS_C_READINGS_NOTIF_ENABLED);
		return err;
	} else {
		LOG_DBG("[READINGS SUBSCRIBED]");
	}

	/* status */
	if (atomic_test_and_set_bit(&nus_c->status_state, NUS_C_STATUS_NOTIF_ENABLED)) {
		return -EALREADY;
	}

	nus_c->status_notif_params.notify = on_status_received;
	nus_c->status_notif_params.value = BT_GATT_CCC_NOTIFY;
	nus_c->status_notif_params.value_handle = nus_c->handles.status;
	nus_c->status_notif_params.ccc_handle = nus_c->handles.status_ccc;
	atomic_set_bit(nus_c->status_notif_params.flags, BT_GATT_SUBSCRIBE_FLAG_VOLATILE);

	err = bt_gatt_subscribe(nus_c->conn, &nus_c->status_notif_params);
	if (err) {
		LOG_ERR("Subscribe status failed (err %d)", err);
		atomic_clear_bit(&nus_c->status_state, NUS_C_STATUS_NOTIF_ENABLED);
		return err;
	} else {
		LOG_DBG("[STATUS SUBSCRIBED]");
	}

		/* result available */
	if (atomic_test_and_set_bit(&nus_c->result_available_state, NUS_C_RESULT_AVAILABLE_NOTIF_ENABLED)) {
		return -EALREADY;
	}

	nus_c->result_available_notif_params.notify = on_result_available_received;
	nus_c->result_available_notif_params.value = BT_GATT_CCC_NOTIFY;
	nus_c->result_available_notif_params.value_handle = nus_c->handles.result_available;
	nus_c->result_available_notif_params.ccc_handle = nus_c->handles.result_available_ccc;
	atomic_set_bit(nus_c->result_available_notif_params.flags, BT_GATT_SUBSCRIBE_FLAG_VOLATILE);

	err = bt_gatt_subscribe(nus_c->conn, &nus_c->result_available_notif_params);
	if (err) {
		LOG_ERR("Subscribe result available failed (err %d)", err);
		atomic_clear_bit(&nus_c->result_available_state, NUS_C_RESULT_AVAILABLE_NOTIF_ENABLED);
		return err;
	} else {
		LOG_DBG("[RESULT AVAILABLE SUBSCRIBED]");
	}

	return err;
}
